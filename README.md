# Demos for interactive VOILA notebooks


## 1. Widget style app

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/georg.ramer%2Fftir-voila-demos/HEAD?urlpath=voila%2Frender%2FFTIR_Resolution-widget.ipynb)

## 2. Gridstack style app

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/georg.ramer%2Fftir-voila-demos/HEAD?urlpath=voila%2Frender%2FFTIR_Resolution-grid.ipynb)

## 3. Run raw notebooks

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/georg.ramer%2Fftir-voila-demos/HEAD)
