{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "57b8e19a-108c-4dd5-9311-40fa8b11d6ad",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": 0,
        "height": 2,
        "hidden": true,
        "row": 0,
        "width": 12
       }
      }
     }
    }
   },
   "source": [
    "# Signal processing in FTIR spectroscopy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4c343a2-2765-4067-b136-57981688daef",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": 0,
        "height": 10,
        "hidden": true,
        "row": 2,
        "width": 5
       }
      }
     }
    }
   },
   "source": [
    "The FTIR spectrometer does not directly record a spectrum, but an interferogram. Before display and further analysis by the analytical chemist this interferogram is [Fourier transformed](https://en.wikipedia.org/wiki/Fourier_transform). \n",
    "\n",
    "Typically, the interferogram is modified numerically before the Fourier transform is calculated. By multiplying the interferogram with a window function or \"apodization\" oscillation around sharp bands can be removed. Zero filling (i.e. adding zeros at the end of the interferogram) improves the apparent spectral resolution by interpolating between points. \n",
    "\n",
    "The image below contains a spectrum with multiple sharp lines (similar to the mid-IR spectrum of a gaseous molecule). You can adjust actual resolution, window function and the zero filling factor.  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f8051a8-4837-4487-b384-56b4e4bc1564",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": 0,
        "height": 10,
        "hidden": true,
        "row": 2,
        "width": 5
       }
      }
     }
    }
   },
   "source": [
    "![FTIR Spectrometer](ftir.jpeg)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67acc62b-8945-4f24-bc37-3a2cc82dd8d5",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": 0,
        "height": 10,
        "hidden": true,
        "row": 2,
        "width": 5
       }
      }
     }
    }
   },
   "source": [
    "## Experiments:\n",
    "\n",
    "1. Adjust the resolution and see how the number of recorded data points changes.\n",
    "2. Reduce the resolution until oscillations start to appear on the base line. How does the interfergram look at this point?\n",
    "3. At this resolution: zoom in on a single band and change the window function. What are advantages and draw backs **boxcar** and **Blackman-Harris** window functions?\n",
    "4. Set the resolution to 2 $\\mathrm{cm^{-1}}$. How does changing the zero filling factor change the interferogram?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "23fb0d5c-ce08-42a6-b03e-d81ecc4d72fe",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy as sp\n",
    "import ipywidgets as widgets\n",
    "import mpl_interactions.ipyplot as iplt\n",
    "import mpl_interactions\n",
    "from scipy.signal import get_window\n",
    "\n",
    "\n",
    "%config InlineBackend.print_figure_kwargs = {'bbox_inches':None}\n",
    "\n",
    "from functools import lru_cache, wraps\n",
    "%matplotlib widget"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "0c0c7776-4dea-46d3-aef0-1bfbce4d009b",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [],
   "source": [
    "\n",
    "def lorentz_ft(delta_l,A, m, w):\n",
    "    f = 2*np.pi*delta_l\n",
    "    return A*np.exp(-np.abs(f*w)/2)*np.exp(-1j*m*f)\n",
    "\n",
    "def lorentzian(x, A, m, w):\n",
    "    return A/(2*np.pi)*w/((x-m)**2 + (1/2*w)**2)\n",
    "\n",
    "def interferogram(res, fun, fft_shift=True):\n",
    "    # res: resolution in 1/cm\n",
    "    spacing = .633E-7 * 5000# cm\n",
    "    delta_l = np.arange(0,1/res, spacing)\n",
    "    delta_l = np.concatenate([delta_l[:-1], -delta_l[-1:0:-1]])\n",
    "    if not fft_shift:\n",
    "        delta_l = np.fft.fftshift(delta_l)\n",
    "    return delta_l, fun((delta_l))*2/res\n",
    "\n",
    "\n",
    "def gas(B, T, bandshape, A0, m0,w,  max_J=6, compl=True):\n",
    "    scaling = 6.626E-34 *3E9 /1.38E-23\n",
    "    def fun(x):\n",
    "        if compl:\n",
    "            out = np.zeros(len(x), dtype=complex)\n",
    "        else:\n",
    "            out = np.zeros(len(x), dtype=float)\n",
    "        for J in range(1,max_J):\n",
    "            scale = (2*J+1)*np.exp(-B/T*scaling *J*(J+1))\n",
    "            out += bandshape(x,A0*scale, m0+(J)*B/100, w)\n",
    "            if J > 1:\n",
    "                out += bandshape(x,A0*scale, m0-(J-1)*B/100, w)\n",
    "        return out\n",
    "    return fun\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a423214c-5b4d-43f9-8964-d5a13ee73f10",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "30563121-9a30-4e64-96b3-b425c93ceee0",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [],
   "source": [
    "class Filter_Visualize:\n",
    "    def __init__(self, fun_ft, fun=None, res=.01, max_wn=4000):\n",
    "        \"\"\"\n",
    "        Hide ugly details for visualizing effects of filters, zerofilling etc for FTIR\n",
    "        \n",
    "        Parameters:\n",
    "        -------------\n",
    "        \n",
    "        fun_ft : function that takes delta_l (in cm) input and returns (complex) amplitude\n",
    "        \n",
    "        fun : function that takes wavenumber input and returns (real) amplitude in wavenumber domain\n",
    "        \n",
    "        res : required resolution (in cm-1)\n",
    "        \n",
    "        max_wn : maximum wavenumber (in cm-1)\n",
    "        \"\"\"\n",
    "        self.fun = fun\n",
    "        self.fun_ft = fun_ft\n",
    "        self.spacing = 633E-7*np.floor(.5/(max_wn*633E-7))\n",
    "        self.res = res\n",
    "        \n",
    "    def attr_exists_and_set(self, attr):\n",
    "        if not hasattr(self, attr):\n",
    "            return False\n",
    "        return getattr(self,attr)  is not None\n",
    "            \n",
    "    @property\n",
    "    def N(self):\n",
    "        if not self.attr_exists_and_set(\"_N\"):\n",
    "            self._N = len(self.delta_l)\n",
    "        return self._N\n",
    "        \n",
    "    \n",
    "    @property\n",
    "    def delta_l(self):\n",
    "        if not self.attr_exists_and_set(\"_delta_l\"):\n",
    "            delta_l = np.arange(0,1/self.res, self.spacing)\n",
    "            self._delta_l = delta_l\n",
    "        return self._delta_l\n",
    "    \n",
    "    @property\n",
    "    def wn(self):\n",
    "        if not self.attr_exists_and_set(\"_wn\"):\n",
    "            self._wn = np.abs(np.fft.fftfreq(len(self.delta_l)*2, \n",
    "                                             self.delta_l[1] - self.delta_l[0])\n",
    "                                             [0:self.N])\n",
    "        return self._wn\n",
    "    \n",
    "    @property\n",
    "    def full_ft(self):\n",
    "        if not self.attr_exists_and_set( \"_full_ft\"):\n",
    "            self._full_ft = self.fun_ft(self.delta_l)*2/self.res\n",
    "        return self._full_ft\n",
    "\n",
    "    @property\n",
    "    def full(self):\n",
    "        if not self.attr_exists_and_set(\"_full\"):\n",
    "            if not self.attr_exists_and_set(\"fun\"):\n",
    "                self._full = sp.fft.idct(self.full_ft.real)\n",
    "            else:\n",
    "                self._full = self.fun(self.wn)\n",
    "        return self._full     \n",
    "    \n",
    "    def resampled_N(self, res):\n",
    "        return int(np.ceil(self.res/res*self.N))\n",
    "    \n",
    "    def res_from_N(self, N):\n",
    "        return 1/(self.spacing*N)\n",
    "    \n",
    "    def undersampled_ft(self, res):\n",
    "        N = self.resampled_N(res)\n",
    "        return self.full_ft[:N]\n",
    "    \n",
    "    def resampled_delta_l(self, res=None, N=None):\n",
    "        if res is not None:\n",
    "            N = self.resampled_N(res)\n",
    "        return self.spacing*np.arange(N)\n",
    "    \n",
    "    def resampled_wn(self, res=None, N=None):\n",
    "        if res is not None:\n",
    "            N = self.resampled_N(res)\n",
    "        return (np.arange(N))/(N*self.spacing*2)\n",
    "    \n",
    "    def undersampled(self, res):\n",
    "        N = self.resampled_N(res)\n",
    "        res = self.res_from_N(N)\n",
    "        return sp.fft.idct(self.undersampled_ft(res).real)/res*self.res\n",
    "    \n",
    "    def window(self, window_function,length=None, ft_data=None):\n",
    "        \"\"\"\n",
    "        window : \"boxcar\", \"triang\", \"blackmanharris\"\n",
    "\n",
    "        \"\"\"\n",
    "        if length is None:\n",
    "            length = len(ft_data)\n",
    "        M = 2*length\n",
    "        window = np.fft.fftshift(get_window(window_function, M))\n",
    "        return window[:length]\n",
    "    \n",
    "    def zero_fill(self, ft_data, zero_filling_factor):\n",
    "        return np.concatenate((ft_data, np.zeros(len(ft_data)*(zero_filling_factor - 1))))\n",
    "    \n",
    "    def undersampled_windowed_ft(self, res, zero_filling_factor=1, window_function=\"boxcar\"):\n",
    "        ft_data = self.undersampled_ft(res)\n",
    "        window_fun = self.window(window_function, ft_data = ft_data)\n",
    "        ft_data = ft_data*window_fun\n",
    "        data_zero_filled = self.zero_fill(ft_data, zero_filling_factor)\n",
    "        return data_zero_filled\n",
    "    \n",
    "    def undersampled_windowed_window(self, res, zero_filling_factor=1, window_function=\"boxcar\"):\n",
    "        ft_data = self.undersampled_ft(res)\n",
    "        window_fun = self.window(window_function, ft_data = ft_data)\n",
    "        ft_data = window_fun\n",
    "        data_zero_filled = self.zero_fill(ft_data, zero_filling_factor)\n",
    "        return data_zero_filled\n",
    "    \n",
    "    def undersampled_windowed_delta_l(self, res, zero_filling_factor=1, window_function=\"boxcar\"):\n",
    "        return self.resampled_delta_l(N= self.resampled_N(res) * (zero_filling_factor))\n",
    "    \n",
    "    def undersampled_windowed_wn(self, res, zero_filling_factor=1, window_function=\"boxcar\"):\n",
    "       \n",
    "        return self.resampled_wn(N= self.resampled_N(res) * (zero_filling_factor))\n",
    "    \n",
    "    def undersampled_windowed(self, res,  zero_filling_factor=1, window_function=\"boxcar\"):\n",
    "        idct = sp.fft.idct(\n",
    "            self.undersampled_windowed_ft(res=res, \n",
    "            zero_filling_factor=zero_filling_factor, \n",
    "            window_function=window_function))\n",
    "        return idct /self.res_from_N(len(idct))*self.res\n",
    "    \n",
    "def dbl_ft(data, freq=False):\n",
    "    scale = 1\n",
    "    if freq:\n",
    "        scale = -1\n",
    "    return np.concatenate((scale*data[-1:0:-1], data[0:]))\n",
    "\n",
    "def dbl_ft_wrap(fun, freq=False):\n",
    "    @wraps(fun)\n",
    "    def wrapper(*args, **kwargs):\n",
    "        return dbl_ft(fun(*args, **kwargs), freq=freq)\n",
    "    return wrapper\n",
    "\n",
    "def de_x(fun):\n",
    "    @wraps(fun)\n",
    "    def wrapper(*args, **kwargs):\n",
    "        fval = fun(*args[1:], **kwargs)\n",
    "        return fval\n",
    "    return wrapper\n",
    "\n",
    "def real(fun):\n",
    "    @wraps(fun)\n",
    "    def wrapper(*args, **kwargs):\n",
    "        fval = fun(*args, **kwargs)\n",
    "        return fval.real\n",
    "    return wrapper"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "7990602a-619e-454f-a2e6-ba8e7b8ac963",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [],
   "source": [
    "#fun = gas(300, 300, lorentzian, .01, 1000, .05, 10)\n",
    "fun_ft = gas(100, 300, lorentz_ft, .01, 1000, .05, 10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "0887a220-3d8b-4940-ae5a-9dbf8eaed916",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [],
   "source": [
    "filter_vis = Filter_Visualize(fun_ft, res=.01, max_wn=1200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "08208516-0fde-41c3-adb7-8de7e7ccc105",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [],
   "source": [
    "plt.close(\"all\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "180323d5-3d18-4bc7-b745-bcb5b0c1460d",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [],
   "source": [
    "layout = widgets.Layout(width=\"auto\")\n",
    "style = {'description_width': '120pt'}\n",
    "controls = mpl_interactions.controller.Controls()\n",
    "controls.add_kwargs({\"res\":widgets.FloatLogSlider(value=.01, \n",
    "                                                 base=10, \n",
    "                                                 min=-2, \n",
    "                                                 max=1, \n",
    "                                                  readout_format=\".2f\", \n",
    "                                                 description=\"Spectral Resolution [cm$^{-1}$]\",\n",
    "                                                 style=style, layout=layout),\n",
    "                    \"window_function\":widgets.Dropdown(options=\n",
    "                                        [(\"boxcar\",\"boxcar\"), (\"triangular\",\"triang\"), (\"Blackman-Harris\",\"blackmanharris\")], \n",
    "                                                       description=\"Window function\",\n",
    "                                                      style=style, layout=layout), \n",
    "                    \"zero_filling_factor\":widgets.IntSlider(value=1, min=1, max=16,\n",
    "                                                            description=\"Zero filling factor\",style=style, layout=layout)})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "36f85a39-5126-43f5-976d-d9faa6f6c9ee",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [],
   "source": [
    "plt.ioff()\n",
    "fig1, ax_ft = plt.subplots(1, squeeze=True, constrained_layout=True, figsize=None)\n",
    "fig2, ax_real = plt.subplots(1, squeeze=True, constrained_layout=True, figsize=None)\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "ax_ft.plot(dbl_ft(filter_vis.delta_l, True), dbl_ft(filter_vis.full_ft.real))\n",
    "ax_window = ax_ft.twinx()\n",
    "\n",
    "iplt.plot(dbl_ft_wrap(filter_vis.undersampled_windowed_delta_l, True),\n",
    "           real(de_x(dbl_ft_wrap(filter_vis.undersampled_windowed_ft))),\n",
    "                     controls=controls,ax=ax_ft,\n",
    "                    xlim=\"fixed\")\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "iplt.plot(dbl_ft_wrap(filter_vis.undersampled_windowed_delta_l, True),\n",
    "           real(de_x(dbl_ft_wrap(filter_vis.undersampled_windowed_window))), \n",
    "          ax=ax_window,\n",
    "          color=\"gray\",\n",
    "          linewidth=2,\n",
    "          controls=controls, ylim=[-1.1,1.1], \n",
    "         label=\"window function\")\n",
    "\n",
    "ax_window.set_yticks([])\n",
    "\n",
    "ax_real.plot(filter_vis.wn, filter_vis.full, label=\"full resolution\")\n",
    "\n",
    "\n",
    "iplt.plot(filter_vis.undersampled_windowed_wn,\n",
    "           real(de_x(filter_vis.undersampled_windowed)), \n",
    "            label=\"filtered\",\n",
    "              ax=ax_real,\n",
    "          controls=controls,\n",
    "         xlim=(980, 1020))\n",
    "\n",
    "ax_real.set_xlabel(r\"$\\mathrm{\\tilde{\\nu}}$ / $\\mathrm{cm^{-1}}$\")\n",
    "ax_real.set_ylabel(\"Intensity / a.u.\")\n",
    "ax_real.set_yticks([])\n",
    "\n",
    "ax_real.set_xlim(980, 1020)\n",
    "handles_labels = []\n",
    "\n",
    "\n",
    "ax_ft.set_xlabel(r\"$\\mathrm{\\Delta l}$ / cm\")\n",
    "ax_ft.set_ylabel(\"Intensity / a.u.\")\n",
    "ax_ft.set_yticks([]);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "2b155b8a-b6a3-4474-8798-b4f43fbea4cd",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [],
   "source": [
    "from itertools import chain\n",
    "fig3=plt.figure( constrained_layout=True, figsize=(2,1))\n",
    "w, h = list(zip(*chain.from_iterable(zip(*ax.get_legend_handles_labels()) for ax in [ax_real, ax_window])))\n",
    "fig3.legend(w, h)\n",
    "fig3.canvas.header_visible=False\n",
    "fig3.canvas.toolbar_visible =False\n",
    "fig3.canvas.width =\"auto\"\n",
    "fig3.canvas.min_width =\"50px\"\n",
    "\n",
    "footer = widgets.HBox([controls.vbox, fig3.canvas])\n",
    "footer.layout.display = \"flex\"\n",
    "footer.layout.justify_content =\"center\"\n",
    "footer.layout.flex_flow = \"row wrap\"\n",
    "footer.layout.min_width = \"80%\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "4207c8a4-90d0-4f4e-9be3-c58eb773dfa4",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "activeView": "grid_default",
      "views": {
       "grid_default": {
        "col": null,
        "height": 2,
        "hidden": true,
        "row": null,
        "width": 2
       }
      }
     }
    }
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "4e4dcc56f23948d8a868111444b98e31",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "AppLayout(children=(HBox(children=(VBox(children=(FloatLogSlider(value=0.01, description='Spectral Resolution …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "for f in [fig1, fig2]:\n",
    "    f.set_size_inches(5, 4, True)\n",
    "    f.canvas.resizable = False\n",
    "    f.canvas.header_visible=False\n",
    "    #f.canvas.layout.border = '1px solid black'\n",
    "    f.canvas.layout.min_width = '100px'\n",
    "    #f.canvas.layout.min_height = '100px'\n",
    "    f.canvas.layout.width = 'auto'\n",
    "    #f.canvas.layout.height = 'auto'\n",
    "    #f.canvas.layout.flex='1 1 auto'\n",
    "    #f.canvas.layout.width = 'auto'\n",
    "    f.canvas.layout.justify_content =\"center\"\n",
    "   \n",
    "    \n",
    "fig_box = widgets.HBox([fig1.canvas, fig2.canvas], )\n",
    "fig_box.layout.display = 'flex'\n",
    "fig_box.layout.flex_flow = \"row wrap\"\n",
    "fig_box.layout.justify_content = \"space-around\"\n",
    "\n",
    "\n",
    "#fig_box\n",
    "\n",
    "widgets.AppLayout(center=fig_box, \n",
    "                  footer=footer,\n",
    "                 merge=True, width=\"100%\")"
   ]
  }
 ],
 "metadata": {
  "extensions": {
   "jupyter_dashboards": {
    "activeView": "report_default",
    "version": 1,
    "views": {
     "grid_default": {
      "cellMargin": 2,
      "defaultCellHeight": 60,
      "maxColumns": 12,
      "name": "grid",
      "type": "grid"
     }
    }
   }
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
